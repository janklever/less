less
====

LESS code

Padrão base para projetos pessoais com regularização com o padrão do Bootstrap 3.1

Default: 'styles.less'

Código em 'default.less'

- GRID
- MEDIA QUERIES
- NORMALIZE
- CODE
- PRINT
- GLYPHICONS (requer arquivos)
- CAROUSEL (requer javascript)
- MODALS (requer javascript)
- POPOVER (requer javascript)
- TOOLTIP (requer javascript)
- ANIMATION
- PROGRESS BAR